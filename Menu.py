import configparser
import subprocess
import curses
import os

def get_program_name_from_path(file_path):
    # Извлечение названия файла без расширения
    return os.path.splitext(os.path.basename(file_path))[0]

def load_programs(file_path):
    config = configparser.ConfigParser()
    config.read(file_path)
    programs = {}
    for key in config['Programs']:
        if key.endswith('_path'):
            program_path = config['Programs'][key]
            program_name_key = key.replace('_path', '_name')
            program_name = config['Programs'].get(program_name_key, get_program_name_from_path(program_path))
            programs[program_name] = program_path
    return programs

def run_menu(stdscr, programs):
    curses.curs_set(0)  # Скрыть курсор
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
    current_row = 0
    max_row = len(programs) - 1

    while True:
        stdscr.clear()
        h, w = stdscr.getmaxyx()

        for idx, program in enumerate(programs):
            x = w // 2 - len(program) // 2
            y = h // 2 - len(programs) // 2 + idx
            if idx == current_row:
                stdscr.attron(curses.color_pair(1))
                stdscr.addstr(y, x, program)
                stdscr.attroff(curses.color_pair(1))
            else:
                stdscr.addstr(y, x, program)
        
        stdscr.refresh()

        key = stdscr.getch()

        if key == curses.KEY_UP and current_row > 0:
            current_row -= 1
        elif key == curses.KEY_DOWN and current_row < max_row:
            current_row += 1
        elif key == curses.KEY_ENTER or key in [10, 13]:
            program_to_run = list(programs.values())[current_row]
            subprocess.Popen(program_to_run, shell=True)

def main():
    programs = load_programs('Config.ini')
    curses.wrapper(run_menu, programs)

if __name__ == "__main__":
    main()
